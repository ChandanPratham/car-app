import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
constructor(private router:Router){}

  searchByBrand(){
    this.router.navigate(['/buy-car/1']);

  }
  
  searchByPrice(){
    this.router.navigate(['/buy-car/2']);

  }
}
