import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn:"root"
})
export class CarBuyService{

    constructor(private http: HttpClient){}

    getBrands(){
        return this.http.get('/carBrand/getBrands');
    }

    getModels(formData: FormData){
        return this.http.post('/carBrand/getModels', formData);
    }

    getModelsByPrice(formData: FormData){
        return this.http.post('/carBrand/getModelsByPrice',formData);
    }
    
}
