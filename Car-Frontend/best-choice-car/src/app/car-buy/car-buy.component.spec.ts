import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarBuyComponent } from './car-buy.component';

describe('CarBuyComponent', () => {
  let component: CarBuyComponent;
  let fixture: ComponentFixture<CarBuyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CarBuyComponent]
    });
    fixture = TestBed.createComponent(CarBuyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
