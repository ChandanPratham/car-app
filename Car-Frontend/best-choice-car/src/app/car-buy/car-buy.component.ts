import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CarBuyService } from './car-buy.service';

@Component({
  selector: 'app-car-buy',
  templateUrl: './car-buy.component.html',
  styleUrls: ['./car-buy.component.scss']
})
export class CarBuyComponent {
  constructor(private router:Router, private route: ActivatedRoute, private carBuyService: CarBuyService ){}
  searchId:number = 0;
  carBrands: string[] = []; // Initialize an array to hold car brands
  selectedValue!: string; // Initialize a variable to hold the selected value
  selectedOption: any;
  showCarsModel: boolean = false;
  selectedCar: string = '';


  
  carDetailsByBrnand: any[] = [];
  ngOnInit(){
    this.searchId = this.route.snapshot.params['id'];
    console.log("SEARCH ID"+this.searchId);
    if(this.searchId == 1){
    this.carBuyService.getBrands()
    .subscribe((response:any)=>{
      this.carBrands= response;
      console.log(this.carBrands);
    }
    )
  }
  }

  findModels(){
    console.log(this.selectedValue);
    const formData = new FormData();
    formData.append('brand', this.selectedValue);

    this.carBuyService.getModels(formData)
    .subscribe((response:any)=>{
      this.carDetailsByBrnand = response
      this.showCarsModel = true;
    })
  }
bookedCar: boolean = false;
  bookCar(){
    console.log(this.selectedCar)
    this.bookedCar = true;
    this.showCarsModel = false;
    this.searchId = 0;
  }
  home(){
    this.router.navigate(['']);
  }

  back(){
    this.showCarsModel = false;
  }

  findModelsByPrice(){
    console.log(this.selectedOption);
    const formData = new FormData();
    formData.append('price', this.selectedOption);

    this.carBuyService.getModelsByPrice(formData)
    .subscribe((response:any)=>{
      this.carDetailsByBrnand = response
      this.showCarsModel = true;
    })
  }
}
